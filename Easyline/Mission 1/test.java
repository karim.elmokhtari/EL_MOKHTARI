import java.util.*;
public class Voyageur {
    private String nom;
    private int age;
    public Voyageur(){}

    public Voyageur(String n, int age){
        this.nom=n;
        this.age=age;
    }

    public String getNom(){
        return nom;
    }
    public void setNom(String nomV){
        this.nom=nomV;
    }
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        System.out.println("Donner votre nom:");

        String nom=sc.next();

        System.out.println("Quel est votre âge?");
        int age=sc.nextInt();
        Voyageur monVoyageur = new Voyageur(nom,age);
      
        //System.out.println("1er voyageur: "+monVoyageur.nom +" "+ monVoyageur.age);
        
        monVoyageur.afficher();

        Voyageur monVoyageur2 = new Voyageur();
        monVoyageur2.nom="Mat";
        monVoyageur2.age=18;
        System.out.println("2e voyageur: "+monVoyageur2.nom +" "+ monVoyageur2.age);

     }
}