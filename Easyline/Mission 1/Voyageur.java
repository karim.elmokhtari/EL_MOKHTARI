import java.util.*;
public class Voyageur {
    private String nom;
    private int age;
    private  String categorie;
    public Voyageur(){}


    public Voyageur(String n, int age){
        Scanner sc=new Scanner(System.in);
        while(n.length()<2){
            System.out.println("nom: ");
            n=sc.next();
        }//w
        while(age<0){
            System.out.println("Donner un âge positif: ");
            age=sc.nextInt();
        }//w
        this.nom=n;
        this.age=age;
       
        if (age>=0 && age<1){
           // System.out.println("nourrisson");
            this.categorie="nourisson";
        }
        if (age>=1 && age<=18){
            this.categorie="enfant";
        }
        if (age>18 && age<=60){
            this.categorie="adulte";
        }
        if (age>60){
            this.categorie="senior";
        }
        //if
         System.out.println("C'est un "+this.categorie);
    }//String



    public String getNom(){
        return this.nom;
    }

    public int getAge(){
        return this.age;
    }

    public String getCategorie(){
        return this.categorie;
    }

    public void setNom(String nomV){
        Scanner sc=new Scanner(System.in);
        while(nomV.length()<2){
            System.out.println("nom: ");
            nomV=sc.next();
        }//w
        this.nom=nomV;
    }//setNom

    public void setAge(int age){
        Scanner sc=new Scanner(System.in);
        while(age<0){
            System.out.println("Donner un âge positif: ");
            age=sc.nextInt();
        }//w
        this.age=age;
    }//setAge

    public void setCategorie(String cat){
        Scanner sc=new Scanner(System.in);
        if (age>=0 && age <1){
           // System.out.println("nourrisson");
            this.categorie="nourisson";
        if (age>=1 && age <=18)
            this.categorie="enfant";
        if (age>18 && age <=60)
            this.categorie="adulte";
        this.categorie=cat;
        if (age>60)
            this.categorie="senior";
        this.categorie=cat;
        }
    }//setCategorie

    public void afficher(){
        System.out.println(this.nom+""+this.age+""+this.categorie);
    }
//fin voyageur

    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);

        System.out.println("Donner votre nom:");
        String nom=sc.next();
        /*while(nom.length()<2){
            System.out.println("Veuillez donner un nom avec plus de 2 lettres: ");
            nom=sc.next();
        }*/

        System.out.println("Quel est votre âge?");
        int age=sc.nextInt();
        /*while(age<0){
            System.out.println("Donner un âge positif: ");
            age=sc.nextInt();
        }*/

        Voyageur monVoyageur = new Voyageur(nom,age);
        //System.out.println("1er voyageur: "+monVoyageur.nom +" "+ monVoyageur.age);
        monVoyageur.afficher();

        Voyageur monVoyageur2 = new Voyageur();
        monVoyageur2.setNom("Karim");
        monVoyageur2.setAge(19);
        monVoyageur2.afficher();
       // System.out.println(monVoyageur2.nom +" "+ monVoyageur2.age);

     }//main

}//class