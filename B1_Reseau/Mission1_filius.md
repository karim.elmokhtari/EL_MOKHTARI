**Karim El Mokhtari**
**B1 réseau mission1**
**16/10/2020 10h10**
**mission 1 fillius**


## Mission 1 : Découverte de la simulation réseau avec Filius

### Fais le 10/10/20

## Etape 0
Filius a bien été installé.

## Etape 1 : Reconstituer le réseau suivant (img/M2-filius0.png) sur Filius (en utilisant un CIDR=16):




Comment  utiliser un CIDR=16, les 16 premiers bits du masque doivent être allumé :

Le masque de sous-réseau codé en binaire est 11111111.11111111.00000000.00000000 .

Le masque de sous-réseau codé en décimal est 255.255.0.0 .





L'adresse IP de M1 (machine 1) est 11000000.10101000.00001010.00001010 en binaire
                               
                               est 192.168.10.10 en décimal

Donc l'adresse de sous-réseau de M1 est 11000000.10101000.00000000.00000000 en binaire.
                                
                                est 192.168.0.0 en décimal.

L'adresse IP de M2 et M3  est 11000000.10101000.00000000.00001010 en binaire
                                
                                est 192.168.0.10 en décimal

Donc l'adresse de sous-réseau de M2 et M3 est 11000000.10101000.00000000.00000000 en binaire.
                                
                                est 192.168.0.0 en décimal.

Au final, nous pouvons voir que les 3 machines (M1,M2 et M3) ont la même adresse de sous-réseau



Les machines qui ont la meme adresse ip ne peuvent pas communiquer entre elles.


### 4.Les machines ne peuvent pas communiquer entre elles car il n'y a manque les éléments d'interconnexion(exemple: le switch ainsi que les cables réseaux)


Sur filisus, la commande pour afficher une adresse ip est ipconfig.


Sur filius, la commande afin de tester la communication est ping.


### Etape 2 : Compléter le réseau précédent comme suit (img/M2-filius1.png)(en utilisant toujours un CIDR=16):



Le média de communication qui a été ajouté est le switch.Il permet de relier grace a des fils d'interconnection les machines pour qu'elles puissent communiquer et échanger des informations entre elles.Le commutateur est intélligent et il apparaît dans la couche 2 du modèle OSI.Il transmet dans plusisueurs ports choisi.
2.Le média de communication qui aurrait pu remplacer le switch est le cencentrateur(HUB).Sa seule différence est de ne pas être intéligent, c'est-à-dire qu'il envoit les informations à tout le monde.Le HUB apparaît dans le niveau 1 (du modèle OSI).




Toutes les machines ne peuvent pas communiquer entre elles car les machines 2 et 3 ont la même adresse IP.




Pour que les machines puissent communiquer entre elles, il faut qu'elles aient tous une adresse IP différente.
Par exemple 192.168.10.10 pour la machine 1, 192.168.0.10 pour la machine 2 et 192.168.1.10 pour la machine 3.




L'adresse à utiliser est l'adresse de diffusion: 192.168.255.255.

### Etape 3 : Compléter le réseau précédent comme suit (img/M2-filius2-bis.png)(en utilisant toujours un CIDR=16):




Les machines M1, M2 et M3 appartiennent au même réseau 192.168.0.10/20 alors que la machine M4 appartient au réseau 192.169.0.10/20




M4 ne peut pas communiquer avec les autres machines car il ne possède pas la même adresse réseau




L'adresse IP de M2 est 192.168.10.10,

L'adresse IP de M4 est 192.169.10.10.

Il faut deja que les deux machines aient la même adresse réseau donc il faut changer l'adresse IP de M2 :

192.168.10.10 --> 192.169.10.10.

Ensuite il ne faut pas que les deux machines aient la même adresse IP ( ce qui signifierai sinpon que ce sont les même machines)

Il faut donc changer un des deux derniers bits de M2 :

192.169.10.10 --> 192.169.1.10 par exemple.

Voila ! Nous avons deux machines : M2 ayant l'adresse IP 192.169.1.10  et M3 ayant l adresse IP 192.169.10.10.

Les deux machines sont prêtes à communiquer.




Maintenant que l'on a changé M2 et que l'adresse réseau n'est plus la même (192.169.1.10 pour M2  et 192.168.10.10 pour M1)les deux machines ne peuvent plus communiquer ensemble car elle n'appartiennent plus au même réseau.


#####  remarque: Meme  M2 avait conservé son adresse réseau 192.168.10.10 , M1 et M2  ne pourraient quand même pas communiquer ensemble car elles possèdaient la même adresse IP.