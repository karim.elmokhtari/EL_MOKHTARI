#Karim El Mokhtari
** Mission 2 **
16/10/2020



                                                         MISSION 2

                                             
                                             
##  Étape 1
1. Quel est le CIDR de chaque machine? Justifier.


Le CIDR des machines est pour :

M1 = 192.168.10.10/16 car le masque possède 16 bits allumés.


M2 = 10.0.1.255/8 car le masque possède 8 bits allumés.


M3 = 10.0.1.245/8 car le masque possède 8 bits allumés.


M4 = 192.168.20.10/16 car le masque possède 16 bits allumés.

2. A quel sous-réseau IP appartient chacune de ces 4 machines? Justificatif

M1 = 255.255.0.0 car le CIDR est égal à 16 bits.


M2 = 255.0.0.0 car le CIDR est égal à 8 bits.


M3 = 255.0.0.0 car le CIDR est égal à 8 bits.


M4 = 255.255.0.0 car le CIDR est égal à 16 bits.

3. Combien de machines maximums peuvent accueillir chacun des sous-réseaux identifiés?

On peut mettre 65 534 machines maximums dans le réseau de M1 et M4 car on fait 2 ^ 16-2.


On peut mettre 16 777 214 machines dans le réseau de M2 ​​et M3 car on fait 2 ^ 24-2.

4. Quelles machines peuvent-elles potentiellement communiquer entre elles? Justifier.

Les machines M1, M2, M3 et M4 peuvent potentiellement communiquer entre elles grâce au switch qui est connecté entre eux.

5. Peuvent-elles dans les conditions physiques présentées ci-dessus communiquer entre elles? Justifier.

Dans les conditions physiques présentées ci-dessus elles peuvent communiqués entre elles car elles sont reliées.

6. Quelle est la commande complète qui permet à M1 de tester la liaison avec M2?

La commande complète qui permet à M1 de tester la liaison avec M2 est ping + adresse IP de la machine.


## Étape 2
1. Quelles sont les adresses IP possibles pour M5 et M6?

Les adresses IP possibles pour M5 et M6 sont 192.168.12.8 et 10.0.1.4

2. Combien de machines peut-on encore ajouter dans chaque sous-réseau?

On peut ajouter 65 534 machines dans le sous-réseau de M5 et 16 777 214 pour le sous réseau de M6.

3. Si M6 veut envoyer un message à toutes les machines de son sous-réseau, quelle adresse IP peut-elle utiliser?

Si M6 veut envoyer un message à toutes les machines de son sous-réseau M6 va devoir utiliser l'adresse de diffusion.

4.  Quel média d'interconnexion est nécessaire pour permettre à toutes les machines d'échanger des messages?

Le média d'interconnexion nécessaire pour permettre à toutes les machines d'échanger des messages est le routeur .


## Étape 3
1.Expliquer brièvement le fonctionnement d'un routeur.

Un routeur est un équipement réseau informatique assurant le routage des paquets. Son rôle est de faire transiter des paquets d'une interface réseau vers une autre, au mieux, selon un ensemble de règles. (Definition wikipédia)

2. Compléter la configuration physique pour permettre aux différentes machines des différents réseaux logiques de communiquer.

Combien d'interfaces réseaux sont nécessaires?


3.  interface réseau sont nécessaires


Quelle adresse IP aura chaque interface?

3. Est-ce que toutes les machines peuvent maintenant communiquer entre elles? Justifier.

Oui toutes les machines peuvent désormais communiquer entre elles parce qu'elles sont reliées à un routeur.

4. Quelle nouvelle configuration est-elle nécessaire pour permettre aux machines de communiquer avec des machines appartenant à d'autres réseaux (M5 avec M6 par exemple)?