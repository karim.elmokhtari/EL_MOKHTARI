package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.content.Intent

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val bt=findViewById<Button>(R.id.activity_hotel_description)
        bt.setOnClickListener{
            //println("youhou")
            val intent = Intent(this, HotelDescriptionActivity::class.java)
            startActivity(intent)
        }
    }
}
