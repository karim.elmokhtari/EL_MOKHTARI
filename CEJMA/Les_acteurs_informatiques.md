# Les acteurs informatiques

### **Question n°1**

**Rechercher l’activité principale (et leur signification si nécessaire) de ces acteurs de l’industrie
informatique**

• **Les constructeurs** : _Un constructeur informatique est une entreprise qui conçoit, développe, produit, commercialise et maintient des plates-formes informatiques, ainsi que les systèmes d'exploitation qui permettent de les faire fonctionner._

• **Les éditeurs de logiciel**s : _Un éditeur de logiciel est une entreprise qui assure la conception, le développement et la commercialisation de produits logiciels. Elle peut confier la mise en œuvre, l'intégration et la personnalisation à des entreprises de services du numérique._

• **Les distributeurs et vendeur de solutions** ;

• **Les intégrateurs** : _Un intégrateur d'infrastructure informatique installe les outils (matériels et logiciels) provenant d'éditeurs et de constructeurs dans un système d'information. Il s'agit d'un informaticien généraliste qui précède et collabore avec l'administrateur réseau._

• **Les infogérants** : _Ce sont des personnes que s'occupe de l'infogérance. L'infogérance (cas particulier d'externalisation) est un service défini comme le résultat d'une intégration d'un ensemble de services élémentaires, visant à confier à un prestataire informatique tout ou une partie du système d'information (SI) d'un client, dans le cadre d'un contrat pluriannuel, à base forfaitaire, avec un niveau de services et une durée définie (définition de l'AFNOR)._

• **Les hébergeurs** : _Un hébergeur web est une entreprise qui fourni l'hébergement sur Internet de systèmes informatiques divers, tel que sites web, stockage d'information, messagerie électronique, etc. à des personnes, associations, projets ou autres entités qui ne désirent pas le faire par leurs propres moyens. Cette hébergement se fait en général au sein d'un parc de serveurs informatiques qui bénéficie 24 heures sur 24 d'une connexion à Internet de haut débit pour offrir un accès rapide aux clients ou à tous les internautes selon le type de service rendu._

• **Les FAI** : _Un fournisseur d'accès à Internet ou FAI (aussi appelé fournisseur de service Internet ou FSI) est un organisme (généralement une entreprise mais parfois aussi une association) offrant une connexion à Internet, le réseau informatique mondial._

• **Les ESN (anciennement appelées SSII)** : _Une entreprise de services du numérique, anciennement société de services en ingénierie informatique, est une société de services experte dans le domaine des nouvelles technologies et de l’informatique._

• **Les services cloud** : _Le terme « services cloud » désigne un large éventail de services fournis à la demande sur Internet aux entreprises et aux clients. Ces services sont conçus pour fournir un accès simple et accessible aux applications et aux ressources, sans qu'une infrastructure interne ou du matériel ne soit nécessaire._


### **Question n°2**

**Définir l’infogérance:** L’infogérance consiste à déléguer la maintenance, la gestion, l’optimisation et la sécurité de votre système d’information à un prestataire informatique.

**Qu’est-ce que la disponibilité :** Le terme «disponibilité» désigne la probabilité qu’un service soit en état de fonctionnement à un instant donné, plus le taux de disponibilité est élevé moins il y a d’interruption de service et donc plus l’équipement est disponible.

**L’infogérance fait l’objet d’un contrat (forfait/régie/annuel). Quelle est la différence ?** Dans le cadre d’un contrat d’infogérance, le prestataire informatique effectue une maintenance préventive sur les serveurs et les équipements réseaux par le biais d’une surveillance matérielle (alimentation, disques durs, mémoire, ventilation…) et fonctionnelle (charge CPU, utilisation RAM, espace disque, état thermique de chaque composant, connectivité, mise à jour logicielle).
Il se differencie aussi grace au service continu de maintenance et d’évolution de votre système d’information.

**Quels sont les objectifs du contrat d’infogérance ?** 

_`Il y a 3 principales objectifs:`_


- Ces actions proactives ont pour principal but de réduire les probabilités de défaillance et
donc limiter les interruptions d’activité en garantissant un haut taux de disponibilité.

- La prestation de l’infogérant consiste également en la mise en place d’une assistance
technique immédiate pour résoudre les éventuelles pannes rencontrées sur les serveurs,
les équipements réseaux et les postes de travail.

-

### **Question n°3**



