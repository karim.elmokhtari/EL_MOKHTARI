<?php





 class PersonneDao implements ManagerInterface {
    private PDO $connection;
    


    
    public function setConnection() {
        $dsn='mysql:dbname=testdb;host=127.0.0.1';
    $user='root';
    $password='';
    try{
        $dbh=new PDO($dsn,$user,$password); 
        $this->connection=$dbh;
    }catch(PDOException $e){
        echo'Connexion échouée:'.$e->getMessage(); 
    }
        
    }
  
    public function insert( $obj):bool {
        $result=false;
        try {

          /*$sql="SELECT max(id)+1 as id FROM personne";
            $statement=$this->connection->query($sql,PDO::FETCH_ASSOC) ;
            $row  = $statement -> fetch();
            $id=$row['id'];*/
            $id=$obj->getId();
            $nom=$obj->getNom();

          $sql="INSERT INTO personne (id, nom) VALUES ( '$id' , '$nom')";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'insert échoué:'.$e->getMessage(); 
        }
        return $result;
    }


    public function select(int $id):Personne {
         $p = new Personne();
        try {
            $sql="SELECT * FROM personne where id = '$id'";
            $statement=$this->connection->query($sql,PDO::FETCH_ASSOC) ;
            $row  = $statement -> fetch();
            $p->setNom($row['nom']);
            $p->setId($row['id']);
        } catch (PDOException $e) {
            echo'select échoué:'.$e->getMessage(); 
        }
        return $p;
    }




public function update( $obj):bool {
        $result=false;
        try {
      $id=$obj->getId();
            $nom=$obj->getNom();
          $sql="UPDATE personne SET nom = '$nom' WHERE id = '$id'";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'update échoué:'.$e->getMessage(); 
        }
        return $result;
 }


public function delete( $obj):bool {
        $result=false;
        try {
            $id=$obj->getId();
          $sql="DELETE FROM personne WHERE id = '$id'";
            echo $sql;
            $nbLignes=$this->connection->exec($sql) ;
            if($nbLignes==1)
              $result=true;
            
        } catch (PDOException $e) {
            echo'delete échoué:'.$e->getMessage(); 
        }
        return $result;
 }

//ici le tableau ne contient pas des objets de type Personne
   public function findAll():array {
        $personnes = null;
        try {
            $sql="SELECT * FROM personne";
            $statement=$this->connection->query($sql);
            $personnes = $statement->fetchAll(PDO::FETCH_OBJ);


        } catch (PDOException $e) {
            echo'select all échoué:'.$e->getMessage(); 
        }
        return $personnes;
    }



}

echo "debut du programme";

$a=new Personne();
$a->setNom("harry");
$a->setId(32);
var_dump($a);

$b=new Personne();
$b->setNom("harold");
$b->setId(31);

$persodao=new PersonneDao();
$persodao->setConnection();
$persodao->insert($a);
$persodao->insert($b);

$a->setNom("jo");
$persodao->update($a);
$unePersonne=$persodao->select(32);
echo "<br><br>".$unePersonne."<br><br>";

$tb=$persodao->findAll();
var_dump($tb);
foreach ($tb as $row ) {
  echo  $row->nom."<br><br>";
 }
 $tb[] = $b;
var_dump($tb);


$persodao->delete($a);


/*CREATE TABLE IF NOT EXISTS `personne` (
  `id` int(11) NOT NULL,
  `nom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
)*/
?>