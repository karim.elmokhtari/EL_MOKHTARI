#Balan Erwan
#04/01/2021
#projet GATO


var port = 3000;
var hostname = 'localhost'
var express = require('express');
var http = require('http');
const { emit } = require('process');
var app = express(); 
var server = http.createServer(app);
var io = require('socket.io')(server);

io.on('connection', function(socket){
  console.log('Karim est Connecté');
  socket.on('disconnect', function(){
    console.log('Karim est Déconnécté');
    io.emit('chat message', msg);
  });
    
  
  socket.on('chat message', function(msg){
    console.log('Karim a envoyé un message: ' + msg);
    io.emit('chat message', msg);
  });
 
});
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

server.listen(port,hostname, function(){ 
  console.log("Mon serveur fonctionne sur http://"+ hostname +":"+port); 
});
