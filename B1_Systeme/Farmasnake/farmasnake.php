<?php
  // Connection à la base de données, avec les paramètres par défaut

$dsn='mysql:dbname=testdb;host=https://sio-hautil.eu';
$user='messim';
$password='messim';
try{
    $dbh=new PDO($dsn,$user,$password); 
}catch(PDOException $e){
    echo'Connexion échouée:'.$e->getMessage(); 
}
?>

<html>

    <head>
         <title>Farmasnake</title>
    </head>
    
    <body>

        <?php

            if( !$recherche )
            /*
            cette ligne est l'équivalent de "if( empty( $recherche ) OR $recherche == 0 )", en bien plus simple, puisque $recherche est un booléen, s'il est vide ou égal à zéro, c'est 'FALSE', donc affichage du moteur, si il est à 1 ou même toute autre valeur, il est 'TRUE', donc exécution de la recherche...
            */
            {
            ?>
            <form name="annuaire" method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
            <?php /* $_SERVER["PHP_SELF"] est la nouvelle syntaxe de $PHP_SELF, et contient le nom du script en cours d'execution */ ?>
            <input type="text"   name="critere"   value="" size="16" />

            <input type="hidden" name="recherche" value="1" />
            <?php
            /*
            l'input 'hidden', invisible, sert à préciser que lorsque le formulaire est posté, il y a une recherche qui est lancée, si on ne met pas cet input, le script afficherait perpétuellement le formulaire...
            cette remarque, et donc cet input masqué, n'est valable que parce que le même document sert à la fois à afficher le formulaire et à traiter les informations. si la propriété ACTION du FORM était un autre script, il n'y aurait pas besoin de préciser le nouvel affichage à faire après le POST...
            */
            ?>

            <input type="submit" name="envoyer" value="Rechercher" />
            <input type="reset" name="annuler" value="Annuler" />
            </form>
            <?php
            }
            else
            {
                $requete = "SELECT alt, href FROM liens WHERE mots LIKE '%" . stripslashes( $critere ) . "%'";

                // exécution de la requête
                $execution  = mysql_query( $requete, $connection ) OR die( "Impossible d'exécuter la requête !" );

                // puis obtention du nombre de lignes retournées par MySQL
                $nbr_tuples = @mysql_num_rows( $execution );

                if( $nbr_tuples > 0 )
                {
                /*
                si il y a un(des) element(s), on le(s) affiche.
                ce test n'est absolument pas obligatoire, surtout si on passe directement au TANT QUE. En effet, une requête qui ne donne aucun résultat n'est pas un requête incorrecte, le DIE ne s'applique pas !
                Cela explique le "arobase" devant mysql_num_rows (c'est le silencieux).
                Si on passe directement au TANT QUE, il n'y pas non plus de message d'erreur, puisque mysql_fetch_array est alors incapable d'avancer au premier élément, donc c'est (déjà) la fin, et aucune ligne n'est affichée...
                Pour conclure, ce SI ne sert qu'à mettre un message "pas d'éléments trouvés" dans le SINON, point barre.
                */
                echo "<b>$nbr_tuples</b> lien(s) trouvé(s).<br />\n";

                while( $un_lien = mysql_fetch_array( $execution ) )
                {
                    echo "<b>- " . $un_lien["alt"] . "</b><br />\n";
                    echo "http://" . $un_lien["href"] . "<br />\n<br />\n";
                }

                mysql_free_result( $execution );
                }
                else
                // affichage d'un message d'erreur
                echo ":'( pas de résultats...<br />\n";
            }
        ?>
    </body>
</html>