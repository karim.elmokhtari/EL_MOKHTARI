# El  Mokhtari Karim
# 26/04/2021

##  1)    Quel est le nom de l'outil?
GLPI


##   2)    Quel est son type de distribution (libre, payant, ...)? coût éventuel?
Logiciel libre


##   3)    Quel type d'application (client lourd, client léger)? OS compatibles?
Tous type de client OS Windows et sur un OS Linux 


##   4)    Citez les principaux modules qui composent l'application et décrivez -les brièvement.

-->Le module Parc permet d'accéder aux différents matériels inventoriés. 


--> Le module Assistance permet de créer, suivre les tickets, et voir les statistiques. Voir


--> Le module Gestion permet de gérer les contacts, fournisseurs, budgets, contrats et documents. Voir Module Gestion.

-->  Le module Outils permet de gérer des notes, la base de connaissance, les réservations, les flux RSS et visualiser les rapports. Voir Module Outils.

--> Le module Administration permet d'administrer les utilisateurs, groupes, entités, profils, règles et dictionnaires. Il permet aussi la maintenance de l'application (sauvegarde et restauration de base, vérifier si une nouvelle version est disponible). Voir Module Administration.

--> Le module Configuration permet d'accéder aux options de configuration générale de GLPI : notifications, collecteurs, tâches automatiques, authentification, plugins, liens externes. Voir Module Configuration.

##   5)	De quoi at-on besoin pour installer l'outil (prérequis matériel, système et applicatif)?
Wamp, paramétré, le redémarré, crée une base de données paramétré php et installation de OUAIP

##   6)	Quels sont, d'après vous, les avantages que peut présenter cette application ?
Les avantages que peut représenter cette application c’est libre, ce n’est pas payant, c’est rapide
