### Karim El Mokhtari

Etape 1:

Résumer en quelques phrases, l'utilité de ce logiciel et ce qu'il permettra de réaliser.

Ce Wireshark est utilisé dans le dépannage et l’analyse de réseaux informatiques, le développement de protocoles, l’éducation et la rétro-ingénierie.


Il permet de décoder les paquets capturés et comprend les différentes structures (encapsulation) des protocoles de communication mais il ne supporte que les types de réseaux supporté par pcap.

Quelles fonctionnalités de Wireshark semblent utiles ?

Parmi les fonctionnalités utiles il y a Websocket et http

Démarrer votre serveur NodeJS (noter l'heure)

Vers 22h50

Démarrer votre client HTTP (navigateur) et se connecter à votre serveur NodeJS (localhost:3000) (noter l'heure)

Vers 22h50

Ecrire quelques messages dans le formulaire de votre application Gato, vérifier que le serveur répond bien (console NodeJS + affichage de votre message sur votre navigateur)

Il a bien repondu sur node.js ainsi que sur le navigateur internet.

Filtrer les paquets HTTP échangés:
Comment différencier les requêtes des réponses ?

Une requête HTTP est un ensemble de lignes envoyé au serveur par le navigateur. Par exemple "a user connected".


Une réponse HTTP est un ensemble de lignes envoyées au navigateur par le serveur. Par exemple "message ...".

Combien de paires (requête et réponse) ont été échangées ?

Il y a 221 paires qui ont été échangées.

Voit-on les messages écrits via Gato dans le contenu des trames ?

Oui

Filtrer les paquets Websocket échangés:
Comment différencier les requêtes des réponses ?
Combien de paires (requête et réponse) ont été échangées ?

Il y a 221 paires qui ont été échangées.

Voit-on les messages écrits via Gato dans le contenu des trames ?

Oui
