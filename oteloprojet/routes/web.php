<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PremierController;
use App\Http\Controllers\ChambreController;
use App\Http\Controllers\ReservationController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/accueil', function () {
    return view('welcome');
});

Route::get('/accueil',[PremierController::class, 'home'] );

Route::get('/monUriVersPremierControllerFunctionhome',[PremierController::class, 'home'] );

Route::get('/chambre',[ChambreController::class, 'store'] );

Route::get('/newReservation',[ReservationController::class, 'create']) ->middleware('auth');
Route::get('/failure',function () {
    return view('failure');
})->name('failure');;

Route::post('/storereservation',[ReservationController::class, 'store'] )->name('reservation.store');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



Route::get('/compte', function () {
    return view('/auth/compte');
});

Route::get('/deconnexion',function () {
    Auth::logout();
     return redirect('/');
 });
 