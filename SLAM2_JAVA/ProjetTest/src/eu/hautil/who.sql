-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id_role` int(3) NOT NULL,
  `libelle` varchar(20) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `roles` (`id_role`, `libelle`) VALUES
(1,	'agent de service'),
(2,	'serveur'),
(3,	'directeur');

DROP TABLE IF EXISTS `utilisateurs`;
CREATE TABLE `utilisateurs` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `mdp` varchar(30) NOT NULL,
  `role` int(3) NOT NULL,
  `date_creation` datetime NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `role` (`role`),
  CONSTRAINT `utilisateurs_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `utilisateurs` (`id`, `login`, `mdp`, `role`, `date_creation`, `nom`, `prenom`) VALUES
(1,	'xforo',	'0123',	3,	'2015-06-05 00:00:00',	'el mokhtari',	'karim'),
(2,	'dadi',	'0123',	2,	'2015-06-25 00:00:00',	'azer',	'azer'),
(3,	'qdfqsf',	'azer',	1,	'2015-12-05 00:00:00',	'lolali',	'lalilo'),
(4,	'dadid',	'0123',	1,	'2005-06-05 00:00:00',	'lolali',	'azer'),
(5,	'francd',	'azer',	2,	'2020-11-02 00:00:00',	'danil',	'daniel'),
(6,	'efsd',	'sdsdf',	2,	'2020-11-02 00:00:00',	'danils',	'daniels'),
(7,	'francdss',	'azer',	1,	'2020-11-02 00:00:00',	'danilss',	'danielss'),
(8,	'esfez',	'zefz',	3,	'2020-11-02 00:00:00',	'danilssd',	'sdgez'),
(9,	'efsds',	'sdsdfss',	3,	'2020-11-02 00:00:00',	'danilste',	'danielssq'),
(10,	'efsdez',	'esfs',	3,	'2020-11-02 00:00:00',	'esfz',	'esdfe');

-- 2021-10-06 09:16:00