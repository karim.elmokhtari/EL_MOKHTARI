
package eu.hautil.histoire3.model;



public class Utilisateur {
    private String login;
    private String mdp;
    private int role;
    private String date_creation;
    private String nom;
    private String prenom;




    public Utilisateur(String login, String mdp, int role, String date_creation, String nom, String prenom) {
        this.login = login;
        this.mdp = mdp;
        this.role = role;
        this.date_creation = date_creation;
        this.nom = nom;
        this.prenom = prenom;
    }

    public String getLogin() {

        return login;
    }

    public void setLogin(String login) {


        this.login = login;
    }

    public String getMdp() {


        return mdp;
    }

    public void setMdp(String mdp) {


        this.mdp = mdp;
    }

    public int getRole() {


        return role;
    }

    public void setRole(int role) {


        this.role = role;
    }

    public String getDate_creation() {


        return date_creation;
    }

    public void setDate_creation(String date_creation) {


        this.date_creation = date_creation;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }





}