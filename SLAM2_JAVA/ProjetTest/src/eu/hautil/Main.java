package eu.hautil;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.*;
import java.util.Scanner;

public class Main{

    public static void main(String[]args){
        fonctions accesRole = new fonctions();

        try {

            System.out.println("Main.java");

            Scanner sc = new Scanner(System.in);
            int choice = 1;

            while(choice>=1 && choice<=4){
                System.out.println("Choisir la tache");
                System.out.println("1 - Voir la table \n" +
                        "2 - Ajouter un élément \n" +
                        "3 - Supprimer un élément \n" +
                        "4 - Sélectionner un rôle en fonction de l'ID \n" +
                        "5 - Quitter");
                System.out.print("La tache : ");
                choice = sc.nextInt();

                switch(choice) {

                    case 1:
                        accesRole.afficherRoles();
                        break;


                    case 2:
                        System.out.println("Renseignez l'ID : ");
                        int id = sc.nextInt();
                        sc.nextLine(); //C'est pour "aspirer" le bouton entrée qui est compté comme une valeur

                        System.out.println("Renseignez le rôle : ");
                        String libelle = sc.nextLine();

                        accesRole.insertRole(id, libelle);
                        accesRole.afficherRolesAfterUpdate();
                        break;


                    case 3:
                        System.out.print("Veuillez rentrer l'ID du rôle que vous souhaitez supprimer : ");
                        int id2 = sc.nextInt();
                        accesRole.deleteRole(id2);

                        accesRole.afficherRolesAfterUpdate();
                        break;


                    case 4:
                        System.out.println("Veuillez rentrer l'ID du rôle que vous souhaitez sélectionner : \n");
                        int id3 = sc.nextInt();
                        accesRole.afficherRoleById(id3);
                        break;


                    case 5:
                        System.out.println("programme terminé");
                        break;

                }
            }


        } catch (SQLException e){
            e.printStackTrace();
        }
    }
}

