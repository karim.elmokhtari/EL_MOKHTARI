package eu.hautil;

import java.sql.*;
import java.util.Scanner;

public class fonctions {

    String driver="com.mysql.jdbc.Driver";//aller sur le site de mySQL (https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-connect-drivermanager.html)
    String urlBDD="jdbc:mysql://localhost:3306/who?";//aller sur le site de mySQL pour voir exemple ( il doit être dans 3306 sinon ça ne marchera pas) (user=minty&password=greatsqldb ceci est une Query String)
    String loginBDD="root";// login de mon serveur Mysql local
    String mdpBDD="root";// mdp de mon serveur Mysql local
    Scanner sc = new Scanner(System.in);

    String req1 = "SELECT id_role, libelle FROM who_roles";
    String req2 = "INSERT INTO who_roles VALUES (4, 'Hotesse')";
    String req3 = "DELETE FROM who_roles WHERE id_role=4 AND libelle='Hotesse'";
    String req4 = "SELECT libelle FROM roles where id_role = ?";



    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        System.out.println("Connexion ok");


        return conn;
    }

    void afficherRoles() throws SQLException {
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        ResultSet beginTable = stmt.executeQuery(req1);
        System.out.println("Table:");
        while (beginTable.next()) {
            System.out.print("Id : " + beginTable.getString(1) + " | ");
            System.out.print("Libelle : " + beginTable.getString(2) + " \n ");
        }
    }

    void insertRole(int id, String libelle) throws SQLException {
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(req2);

        pstmt.setInt(1, id);
        pstmt.setString(2, libelle);

        int result2 = pstmt.executeUpdate(); //INSERT
        System.out.println("Vos données ont bien été inserées. Le nombre de lignes modifiées est de : " + result2 + "\n");

        ResultSet result3 = stmt.executeQuery(req1); //Récupère la table role après le INSERT
        System.out.println("Voici la table après vos modification : ");
        while (result3.next()) {
            System.out.print("Id : " + result3.getString(1) + " | ");
            System.out.print("Libelle : " + result3.getString(2) + " \n ");
        }

        result3.close();
    }

    void deleteRole(int id) throws SQLException {

        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt2 = conn.prepareStatement(req3);
        pstmt2.setInt(1, id);

        int result5 = pstmt2.executeUpdate();



    }


    void afficherRoleById(int id) throws SQLException {
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        //Statement stmt = conn.createStatement();
        PreparedStatement pstmt3 = conn.prepareStatement(req4);
        pstmt3.setInt(1, id);

        ResultSet result6 = pstmt3.executeQuery();
        while (result6.next()) {
            System.out.print("Id : " + result6.getString(1));
        }
        result6.close();
    }

    void afficherRolesAfterUpdate() throws SQLException {
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        Statement stmt = conn.createStatement();
        ResultSet beginTable = stmt.executeQuery(req1); //Récupère la table role après le INSERT
        System.out.println("\n");
        System.out.println("Voici la table après vos modifications : ");
        while (beginTable.next()) {
            System.out.print("Id : " + beginTable.getString(1) + " | ");
            System.out.print("Libelle : " + beginTable.getString(2) + " \n ");
        }
    }

}


// Les erreurs : ClassNotFoundException ( Driver pas trv )
// On a copié le jar dans projet pour faciliter les déplacements du projet ( celui du dossier mySQL connector )
