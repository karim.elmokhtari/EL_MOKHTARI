package eu.hautil;

import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class Main {

    public static void main(String[] args) {
        System.out.println("hello JDBC");
        // la page : https://dev.mysql.com/doc/connector-j/5.1/en/connector-j-usagenotes-connect-drivermanager.html
        String driver = "com.mysql.cj.jdbc.Driver"; //aller sur le site de mysql(nom de la classe driver)
        String urlBDD = "jdbc:mysql://localhost:3306/DM1"; //aller sur le site de mysql pour voir l'exemple   queryString : ?user=minty&password=greatsqldb
        String loginBDD = "root"; //login de mon serveur MySQL local
        String mdpBDD = "root"; //mdp de mon serveur mysql local
        String req1 = "SELECT id_role, libelle FROM roles";
        String req2 = "INSERT INTO roles VALUES (3, 'serveur')";
        String req3 = "DELETE FROM roles WHERE id_role=3 AND libelle='serveur'";



        try {
            Class.forName(driver); //charger la classe en mémoire
            Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
            Statement stmt = conn.createStatement();
            System.out.println("Connection ok"+ conn);

            ResultSet result = stmt.executeQuery(req1);
            while(result.next()){
                System.out.println("Id_roles : " + result.getInt(1));
                System.out.println("libelle : " + result.getString(2)) ;
            }

            // 1er resultat

            int res = stmt.executeUpdate(req2);

            ResultSet result2 = stmt.executeQuery(req1);

            while(result2.next()){
                System.out.println("Id_roles : " + result2.getInt(1));
                System.out.println("libelle : " + result2.getString(2) );
            }

            int res2 = stmt.executeUpdate(req3);

            // 2e resultat

            ResultSet result3 = stmt.executeQuery(req1);

            while(result3.next()){
                System.out.println("Id_roles : " + result2.getInt(1));
                System.out.println("libelle : " + result2.getString(2) );
            }

            // 3e resultat

            result.close();
            result2.close();
            result3.close();
            stmt.close();



            conn.close();


        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}

// Les erreurs : ClassNotFoundException ( Driver pas trv )
// On a copié le jar dans projet pour faciliter les déplacements du projet ( celui du dossier mySQL connector )




/*import java.sql.Connection;
import java.sql.DriverManager;
public class TestJDBC {
public static void main(String[] args) {
try{
String driver="com.mysql.jdbc.Driver"; //disponible sur le site MySQL
String urlBDD="jdbc:mysql://[serveur:port]/[maBase]";
String user="login"; //login de connexion au serveur MySQL
String pwd="mdp"; //mdp correspondant
Class.forName(driver);
System.out.println("driver ok");
Connection conn= DriverManager.getConnection(urlBDD,user,pwd);
System.out.println("connection ok");
//suite du code
…
}catch(SQLException | ClassNotFoundException e){
e.printStackTrace();
}
}} */
