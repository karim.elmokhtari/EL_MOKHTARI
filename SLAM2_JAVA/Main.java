package eu.hautil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) {
        System.out.println("pied");
        String driver="com.mysql.jdbc.cj.Driver";//aller sur le site de mysql(voir nom complet de la classe driver)
        String urlBDD="jdbc:mysql://localhost/el_mok"; //sio-hautil.eu:3306/el_mok"+"user=minty&password=el_mok"(QUERY STRING)
        String loginBDD="root";//login de mon serveur mysql local
        String mdpBDD="root";// mdp de mon serveur mysql local

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");//charger la classe en memoire
            Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);
            System.out.println("connection ok" + conn);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        //classNotFoundException: le driver n'a pas été trouvé:il faut le recuperer et le lier au projet
        //copier jar dans projet pour en faciliter les déplacements du projet
    }
}
